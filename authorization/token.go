package authorization

import (
	"fmt"
)

func GetAppToken(svcName string, svcSecret string) (string, error) {
	return fmt.Sprintf(`{"iss":"sparrow_cloud_go","uid":"%s","src":"%s"}`, svcName, svcName), nil
}

func GetUserToken(svcName string, svcSecret string, userID string) (string, error) {
	return fmt.Sprintf(`{"iss":"sparrow_cloud_go","uid":"%s","src":"%s"}`, userID, svcName), nil
}
