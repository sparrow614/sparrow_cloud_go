package authorization

import (
	"testing"

	"gitee.com/sparrow614/sparrow_cloud_go/middleware/auth"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/httptest"
)

func TestTokenHeader(t *testing.T) {
	var app = iris.New()

	handlePing := func(ctx iris.Context) {
		p := ctx.Values().Get(auth.DefaultClaimsKey)
		if p == nil {
			ctx.StatusCode(iris.StatusInternalServerError)
			ctx.JSON(iris.Map{"message": "not found payload in middleware"})
			return
		}
		data, ok := p.(map[string]interface{})
		if !ok {
			ctx.StatusCode(iris.StatusInternalServerError)
			ctx.JSON(iris.Map{"message": "payload is not map type"})
			return
		}
		ctx.JSON(data)
	}

	app.Get("/secured/ping", auth.IsAuthenticated, handlePing)
	e := httptest.New(t, app)

	e.GET("/secured/ping").Expect().Status(iris.StatusUnauthorized)

	svcName := "MockSvcName"
	token, _ := GetAppToken(svcName, "MockSecret")
	res := e.GET("/secured/ping").WithHeader("X-Jwt-Payload", token).Expect().Status(iris.StatusOK)
	res.JSON().Object().ContainsKey("uid").Value("uid").String().Equal(svcName)
	res.JSON().Object().ContainsKey("src").Value("src").String().Equal(svcName)

	uid := "MockUserID"
	token, _ = GetUserToken(svcName, "MockSecret", uid)
	res = e.GET("/secured/ping").WithHeader("X-Jwt-Payload", token).Expect().Status(iris.StatusOK)
	res.JSON().Object().ContainsKey("uid").Value("uid").String().Equal(uid)
	res.JSON().Object().ContainsKey("src").Value("src").String().Equal(svcName)
}
