package auth

import (
	"encoding/base64"
	"encoding/json"

	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/alias"
	"gitee.com/sparrow614/sparrow_cloud_go/utils/logging"
	"github.com/kataras/iris/v12"
)

var logger = logging.GetLogger()

type AuthFunc = func(ctx iris.Context) *User

const (
	// DefaultAuthKey is the default key for the token in the request header.
	DefaultAuthKey        = "X-Jwt-Payload"
	DefaultAuthPayloadKey = "AUTH_PAYLOAD"
)

/*
如果授权函数返回有效的用户, 则设置到ctx.User()中
如果授权还是没提供或者没有返回有效用户, 则设置一个空的用户到ctx.User()中
空的User默认为未授权状态
*/
func AuthMiddlewareFunc(af AuthFunc) iris.Handler {
	return func(ctx iris.Context) {
		var user *User
		if af != nil {
			user = af(ctx)
		}
		if user == nil {
			user = &User{
				UserModel:    nil,
				Id:           "",
				Username:     "",
				Phone:        "",
				IsAuthorized: false,
			}
		}
		ctx.SetUser(user)
		ctx.Next()
	}
}

/*
默认提供授权函数
*/
func UserIDAuth(ctx iris.Context) *User {
	rawData := ctx.GetHeader(DefaultAuthKey)
	logger.Debug(ctx, "[AUTH] get X-Jwt-Payload=", rawData)
	user := &User{
		UserModel:    nil,
		Id:           "",
		Username:     "",
		Phone:        "",
		IsAuthorized: false,
	}
	if rawData == "" {
		return user
	}
	var payload alias.Map
	// 这里要使用 RawURLEncoding，而不是 StdEncoding. jwt对于 base64url 的编码方式有要求
	b64Payload, err := base64.RawURLEncoding.DecodeString(rawData)
	// b64Payload, err := base64.StdEncoding.DecodeString(rawData)
	if err != nil {
		// base64解码出错，再次尝试进行文本解析
		// utils.LogDebugf(ctx, "[AUTH] base64 decode fail: %s, try text decode...\n", err)
		logger.Debug(ctx, "[AUTH] base64 decode fail: ", err, ". try text decode...")
		err = json.Unmarshal([]byte(rawData), &payload)
		if err != nil {
			logger.Debug(ctx, "[AUTH] can not decode X-Jwt-Payload:, error:", rawData, err)
			return user
		}
	} else {
		// base64解码成功
		err = json.Unmarshal(b64Payload, &payload)
		if err != nil {
			logger.Debug(ctx, "[AUTH] unmarshal base64 data: to map type fail: ", b64Payload, err)
			return user
		}
	}
	// 存储payload至中间件
	ctx.Values().Set(DefaultAuthPayloadKey, b64Payload)
	// 获取uid
	id, ok := payload["uid"].(string)
	if !ok || id == "" {
		logger.Debug(ctx, "[AUTH] uid not found in payload: %v\n", payload)
		return user
	}
	user.Id = id
	user.IsAuthorized = true
	return user
}

/*
 * 获取系统用户
 */
func GetUser(ctx iris.Context) *User {
	user, ok := ctx.User().(*User)
	if ok {
		return user
	}
	logger.Error("user is not *User type")
	// 返回空 未授权的 User
	user = &User{
		UserModel:    nil,
		Id:           "",
		Username:     "",
		Phone:        "",
		IsAuthorized: false,
	}
	return user
}

// 登录请求中间件
func LoginRequire(ctx iris.Context) {
	user := GetUser(ctx)
	logger.Debug("user----------: ", user)
	if !user.IsAuthorized {
		ctx.StatusCode(iris.StatusUnauthorized)
		ctx.JSON(alias.Map{
			"code":    -1,
			"message": "未登录(-6)",
		})
		ctx.StopExecution()
		return
	}
	ctx.Next()
}
