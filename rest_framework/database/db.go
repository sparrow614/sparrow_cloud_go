package database

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

// 全局唯一的db对象
// var db *gorm.DB

type Db = gorm.DB

var db *Db

type Config struct {
	UserName string // 数据库用户名
	Password string // 密码
	Host     string // 主机地址
	Port     int    // 端口号
	DbName   string // 数据库名称
	SqlDebug bool   // 是否打印sql语句
}

func Init(c *Config) {
	db = NewDb(c)
}

// InitDb init mysql database connection
func NewDb(c *Config) *Db {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=true&loc=Local", c.UserName,
		c.Password, c.Host, c.Port, c.DbName)
	var mlogger logger.Interface
	if c.SqlDebug {
		mlogger = logger.Default.LogMode(logger.Info)
	} else {
		mlogger = logger.Default.LogMode(logger.Silent)
	}
	// var logger
	gdb, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		//日志级别
		Logger: mlogger,
		NamingStrategy: schema.NamingStrategy{
			// TablePrefix: "t_",   // table name prefix, table for `User` would be `t_users`
			SingularTable: true, // use singular table name, table for `User` would be `user` with this option enabled
			// NoLowerCase: true, // skip the snake_casing of names
			// NameReplacer: strings.NewReplacer("CID", "Cid"), // use name replacer to change struct/field name before convert it to db name
		},
	})
	if err != nil {
		panic("failed to connect database" + err.Error())
	}
	return gdb
}

func GetDb() *Db {
	if db == nil {
		panic("db 没有初始化")
	}
	return db
}
