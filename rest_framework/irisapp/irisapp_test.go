package irisapp

import (
	"encoding/base64"
	"encoding/json"
	"testing"
	"time"

	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/alias"
	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/auth"
	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/cache"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/httptest"
)

// 测试参考
// https://github.com/kataras/iris/blob/master/_examples/testing/httptest/main_test.go

const (
	USER_ID = "testid"
)

var (
	payload = alias.Map{
		"uid": USER_ID,
		"exp": time.Now().Unix() + 3600,
		"iat": time.Now().Unix(),
	}
)

// type MyUserModel struct {
// 	Id string
// }

// func (m *MyUserModel) GetID() string {
// 	return m.Id
// }

// func (m *MyUserModel) GetUsername() string {
// 	return m.Id
// }

// func (m *MyUserModel) IsAuthorized() bool {
// 	return true
// }

// func (m *MyUserModel) GetPhone() string {
// 	return ""
// }

// func testUserID(ctx iris.Context) *auth.User {
// 	ctx.User()
// 	userId := ctx.GetHeader(jwt.DefaultUserIDKey)
// 	um := MyUserModel{
// 		Id: userId,
// 	}
// 	user := &auth.User{}
// 	user.SetUserModel(&um)
// 	return user
// }

// 生成token
// func genToken() string {
// 	// secret := settings.GetString("JWT_SECRET")
// 	expireIn := time.Duration(3600) * time.Second
// 	issuer := ""
// 	token := jwtv1. .GenTokenHS256(USER_ID, expireIn, issuer)
// 	return token
// }

/*
	测试一个空的App, 不填加任何组件
*/

func TestApp1(t *testing.T) {
	c := IrisAppConfig{
		SettingsName: "test_settings",
	}
	app := NewIrisApp(&c)
	app.Get("/api/ping", func(ctx iris.Context) {
		ctx.JSON(iris.Map{
			"message": "ok",
			"code":    0,
		})
	})
	e := httptest.New(t, app)
	e.GET("/api/ping").Expect().Status(httptest.StatusOK).JSON().Object().HasValue("code", 0)
}

/*
描述: 测试AuthFunc: UserIDAuth
参数:

	AuthFunc: UserIDAuth

结果

	http status: httptest.StatusOk
	remote_user: "testid"
*/
func TestApp2(t *testing.T) {
	c := IrisAppConfig{
		SettingsName: "test_settings",
		AuthFunc:     auth.UserIDAuth,
	}

	app := NewIrisApp(&c)

	app.Get("/api/ping", func(ctx iris.Context) {
		// remoteUser := ctx.GetHeader(jwt.DefayktUserIDKey)
		// fmt.Println("remoteUser=", remoteUser)
		user := auth.GetUser(ctx)
		// userID, _ := user.GetID()
		ctx.JSON(user)
	})
	e := httptest.New(t, app)
	// 直接在header设置授权后的 tokenPayload string
	// tokenPayload := `{"uid":` + userId + `,"username": "","phone":"","is_authorized":true}`
	// 将tokenPayload转换为使用base64编码的字符串
	// tokenPayload = base64.StdEncoding.EncodeToString([]byte(tokenPayload))
	tokenPayload := GenBase64Token(payload)

	// token := jwt.GenTokenHS256(secret, USER_ID, expireIn, issuer)
	// token := genToken()
	// tokenmsg := fmt.Sprintf("Bearer %s", token)
	// fmt.Println(tokenmsg)
	e.GET("/api/ping").WithHeader(auth.DefaultAuthKey, tokenPayload).Expect().
		Status(httptest.StatusOK).JSON().Object().HasValue("id", USER_ID).HasValue("is_authorized", true)
}

/*
测试用例: 测试自定义 User Model
自定义 User Model: MyUserModel
参数:

	token: 有效
	AuthFunc: 返回 nil

结果

	http status: httptest.StatusOk
	remote_user: "testid"
*/

type MyUserModel struct {
	Id         string `json:"id"`
	Promission string `json:"promission"`
}

func (m *MyUserModel) GetID() string {
	return m.Id
}

// 重写授权方法, 将自定义的 User Model 设置到 User 中
func myUserIDAuth(ctx iris.Context) *auth.User {
	user := auth.UserIDAuth(ctx)
	userModel := &MyUserModel{
		Id:         "",
		Promission: "admin",
	}
	user.SetModel(userModel)
	return user
}

func TestApp3(t *testing.T) {
	c := IrisAppConfig{
		SettingsName: "test_settings",
		// CacheType:    cache.CacheTypeMem,
		AuthFunc: myUserIDAuth,
		EnableDb: false,
	}
	app := NewIrisApp(&c)
	app.Get("/api/ping", func(ctx iris.Context) {
		user := auth.GetUser(ctx)
		ctx.JSON(user)
	})
	tokenPayload := GenBase64Token(payload)
	e := httptest.New(t, app)
	e.GET("/api/ping").WithHeader(auth.DefaultAuthKey, tokenPayload).Expect().Status(httptest.StatusOK).JSON().
		Object().Value("user_model").Object().HasValue("promission", "admin")
}

/*
	测试cache
*/

func TestApp4(t *testing.T) {
	testData := "testData"
	testKey := "TEST"

	c := IrisAppConfig{
		SettingsName: "test_settings",
		CacheType:    cache.CacheTypeMem,
		EnableDb:     false,
	}
	app := NewIrisApp(&c)
	app.Get("/api/ping", func(ctx iris.Context) {
		// user := auth.GetUser(ctx)
		ca := cache.GetCache()
		x := time.Second * 20
		ca.Set(testKey, "testData", x)
		re, err := ca.Get(testKey)
		ctx.JSON(alias.Map{
			"result": re,
			"err":    err,
		})
	})
	e := httptest.New(t, app)
	// token := genToken()
	// tokenmsg := fmt.Sprintf("Bearer %s", token)
	e.GET("/api/ping").Expect().Status(httptest.StatusOK).JSON().Object().HasValue("result", testData)
}

// 测试 Options 方法的跨域
func TestAppOptions(t *testing.T) {
	c := IrisAppConfig{
		SettingsName: "test_settings",
	}
	app := NewIrisApp(&c)
	app.Get("/api/ping", func(ctx iris.Context) {
		ctx.JSON(iris.Map{
			"message": "ok",
			"code":    0,
		})
	})
	e := httptest.New(t, app)
	e.OPTIONS("/api/ping").WithHeader("Access-Control-Request-Method", "GET").Expect().Status(httptest.StatusOK).JSON().Object().HasValue("code", 0)
}

func GenBase64Token(payload alias.Map) string {
	data, err := json.Marshal(payload)
	if err != nil {
		return ""
	}
	// 将tokenPayload转换为使用base64编码的字符串, 使用base64url编码, 后面不补=
	return base64.RawURLEncoding.EncodeToString(data)
	// return base64.URLEncoding.EncodeToString(data)
}
