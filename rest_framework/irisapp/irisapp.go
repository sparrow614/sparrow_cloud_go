package irisapp

import (
	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/auth"
	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/cache"
	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/database"
	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/setting"
	"gitee.com/sparrow614/sparrow_cloud_go/utils/logging"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/recover"
)

/*
1. 创建 iris app
2. 设置日志级别
3. 设置 recover 中间件
4. 设置 logger 中间件
5. 设置数据库
6. 设置缓存: 默认使用 memcache
@params settingsName 配置文件名称
*/
func NewIrisApp(c *IrisAppConfig) *iris.Application {
	if c == nil {
		c = &IrisAppConfig{}
	}
	// 初始化配置文件
	if c.SettingsName == "" {
		c.SettingsName = "settings.yaml"
	}
	setting.Init(c.SettingsName)
	// 初始化 logger
	debug := setting.GetBool("DEBUG")
	// logging.Init(debug)
	// 初始化app
	app := iris.New()
	if debug {
		app.Logger().SetLevel("debug")
	} else {
		app.Logger().SetLevel("info")
		logging.GetLogger().SetLevel(logging.InfoLevel)
	}
	logging.GetLogger().Info("初始化app: DEBUG=", debug)
	app.Use(recover.New())

	// 初始化数据库
	if c.EnableDb {
		dbConfig := database.Config{
			UserName: setting.GetString("DATABASE_USER"),
			Password: setting.GetString("DATABASE_PASSWORD"),
			Host:     setting.GetString("DATABASE_HOST"),
			Port:     setting.GetInt("DATABASE_PORT"),
			DbName:   setting.GetString("DATABASE_DBNAME"),
			SqlDebug: setting.GetBool("SQL_DEBUG"),
		}
		// 检测参数放在这里, 可以让调用者知道是哪个环境变量没有设置
		if dbConfig.UserName == "" {
			panic("数据库参数: DATABASE_USER 没有设置")
		}
		if dbConfig.Password == "" {
			panic("数据库参数: DATABASE_PASSWORD 没有设置")
		}
		if dbConfig.Port == 0 {
			panic("数据库参数: DATABASE_PORT 没有设置")
		}
		if dbConfig.DbName == "" {
			panic("数据库参数: DATABASE_DBNAME 没有设置")
		}
		database.Init(&dbConfig)
		logging.GetLogger().Info("数据库初始化成功")
	}
	// 初始化缓存
	if c.CacheType == cache.CacheTypeRedis {
		redisConfig := cache.ConfigRedis{
			Host:     setting.GetString("REDIS_HOST"),
			Password: setting.GetString("REDIS_PASSWORD"),
			Db:       setting.GetInt("REDIS_DB"),
		}
		if redisConfig.Host == "" {
			panic("数据库参数: REDIS_HOST 没有设置")
		}
		if err := cache.InitRedis(&redisConfig); err != nil {
			logging.GetLogger().Error("初始化redis失败", err)
			panic(err)
		}
		logging.GetLogger().Info("缓存初始化成功: CacheTypeRedis")
	} else if c.CacheType == cache.CacheTypeMem {
		// 使用默认的memcache
		cache.InitMem()
		logging.GetLogger().Info("缓存初始化成功: CacheTypeMem")
	}
	// 接口允许options方法
	app.AllowMethods(iris.MethodOptions)
	// 允许跨域
	opt := cors.Options{
		AllowedOrigins:     []string{"*"},
		AllowedMethods:     []string{"HEAD", "GET", "POST", "PUT", "PATCH", "DELETE"},
		AllowedHeaders:     []string{"*"},
		AllowCredentials:   true,
		OptionsPassthrough: true,
		Debug:              true,
	}
	// crs := cors.AllowAll()
	crs := cors.New(opt)
	app.UseRouter(crs)
	logging.GetLogger().Info("设置允许跨域")
	// 初始化swagger
	// if c.EnableSwagger {
	// 	config := &swagger.Config{
	// 		// The url pointing to API definition.
	// 		URL: c.settings.GetString("SWAGGER_URL"),
	// 		// DeepLinking: true,
	// 	}
	// 	swaggerUI := swagger.CustomWrapHandler(config, swaggerFiles.Handler)
	// 	app.Get("/swagger", swaggerUI)
	// 	app.Get("/swagger/{any:path}", swaggerUI)
	// }
	// if c.EnableJwt {
	// 	// secret := settings.GetString("JWT_SECRET")
	// 	jwtMiddleware := irfauth.GetJwtAuth()
	// 	app.Use(jwtMiddleware.Serve)
	// }
	// 初始化auth
	// 如果没有提供 AuthFunc, 则默认为空
	if c.AuthFunc != nil {
		auth := auth.AuthMiddlewareFunc(c.AuthFunc)
		app.Use(auth)
		logging.GetLogger().Info("设置AuthFunc: ")
	}
	// 默认404
	if c.NotFoundHandler == nil {
		app.OnErrorCode(iris.StatusNotFound, notFound)
		logging.GetLogger().Info("设置 404 handler: ")
	}
	// 默认500
	if c.InternalServerErrorHandler == nil {
		app.OnErrorCode(iris.StatusInternalServerError, internalServerError)
		logging.GetLogger().Info("设置 500 handler: ")
	}
	// logger.Debug("here")
	return app
}

func notFound(ctx iris.Context) {
	ctx.StatusCode(iris.StatusNotFound)
	ctx.JSON(iris.Map{
		"code":    -1,
		"message": "请求的资源不存在",
	})
}

func internalServerError(ctx iris.Context) {
	ctx.JSON(iris.Map{
		"code":    -1,
		"message": "error",
	})
}
