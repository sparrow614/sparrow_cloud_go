package irisapp

import (
	"gitee.com/sparrow614/sparrow_cloud_go/rest_framework/auth"
	"github.com/kataras/iris/v12"
)

type IrisAppConfig struct {
	// setting 名字, 默认值 settings.yaml
	SettingsName string
	// 默认值: notFound
	NotFoundHandler iris.Handler
	// 默认值: internalServerError
	InternalServerErrorHandler iris.Handler
	// 是否启用swagger, 默认值: false, swagger 无法做到完全与main函数分离,不在这里初始化
	// EnableSwagger bool
	// 启用cache, 选项: cache.CacheTypeMem, cache.CacheTypeRedis, 默认值: cache.CacheTypeMem
	CacheType string
	// 是否初始化数据库, 默认值: false
	EnableDb bool
	// Auth处理函数, 默认值: nil
	AuthFunc auth.AuthFunc
	// 启用jwt中间件
	// EnableJwt bool
	// debug
	Debug bool // 默认值 false
}
