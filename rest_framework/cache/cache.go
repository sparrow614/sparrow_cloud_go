package cache

import (
	"time"

	"gitee.com/sparrow614/sparrow_cloud_go/utils/logging"
)

var logger = logging.GetLogger()

var cacheV1 ICache

// var NotInitCache = errors.New("cache 没有初始化")

const (
	CacheTypeMem   = "mem"
	CacheTypeRedis = "redis"
)

// 抽象 cache 类
type ICache interface {
	Set(key string, val interface{}, expiration time.Duration) error
	Get(key string) (string, error)
	GetBytes(key string) ([]byte, error) // 获取缓存的 bytes
	// GetKey() string                      // 生成key值
	Ping() (string, error)
	Close() error // 关闭连接
}

// 全局唯一
func GetCache() ICache {
	if cacheV1 == nil {
		logger.Fatal("cache 没有初始化")
	}
	return cacheV1
}

func InitRedis(c *ConfigRedis) error {
	ca, err := NewRedisCache(c)
	if err != nil {
		return err
	}
	_, err = ca.Ping()
	if err != nil {
		return err
	}
	cacheV1 = ca
	return nil
}

func InitMem() {
	ca := NewMemCache()
	cacheV1 = ca
}
